<?php

ini_set("memory_limit", "3000M");

define("EOL", "\n");

require_once('./extras/utils/PHPExcel.php');
require_once('./extras/utils/PHPExcel/IOFactory.php');
require_once(DIR_APLI_ADMIN . '/models/model.php');


$dir = $_SERVER['DOCUMENT_ROOT'] . '/extras/protected_files/';

$file_name = 'cataleg-' . date('Ymd-His') . '.xlsx';
//pr($_FILES['file']);die;
rename($_FILES['file']['tmp_name'], $dir . $file_name);

echo "Inici proces importacio", EOL;

$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objPHPExcel = $objReader->load($dir . $file_name);

$hoy = date('Y-m-d H:i:s');

$m = new proves();

// Borrem tots els elements actuals de la base de dades
$m->delete_table();


foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {


//    print_r($objPHPExcel);die;
//    echo 'Worksheet - ' . $worksheet->getTitle();

    $values = '';

    $count = 0;

    foreach ($worksheet->getRowIterator() as $row) {
//        print_r($row);die();
        if ($row->getRowIndex() > 1) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false);

            $area = "''";
            $code = "''";
            $description = "''";
            $method = "''";
            $test = "''";
            $test_quantity = "''";
            $test_tube_type = "''";
            $response_time = "''";
            $reference_values = "''";

//            $values = '';

            foreach ($cellIterator as $cell) {

                $id_col = substr($cell->getCoordinate(), 0, 1);
                if (strlen($cell->getValue()) != 0) {
//                    print_r($id_col." ".$cell->getValue());die;

                    // Área de coneixement
                    if ($id_col == "A") {
                        if ($cell->getValue() != "Área de coneixement") {
                            $area = '\'' . $cell->getValue() . '\'';

                        }

                    }
                    // Código
                    if ($id_col == "B") {
                        if ($cell->getValue() != "CODIG") {
                            $code = '\'' . $cell->getValue() . '\'';
                        }
                    }

                    // Descripció
                    if ($id_col == "C") {
                        if ($cell->getValue() != "DESCRIPCIÓ") {
                            $description = '\'' . mysql_real_escape_string($cell->getValue()) . '\'';
//                            print_r($description);die;
                        }

                    }

                    if ($id_col == "D") {
                        if ($cell->getValue() != "Mètode") {
                            $method = '\'' . mysql_real_escape_string($cell->getValue()) . '\'';

                        }
                    }

                    if ($id_col == "E") {
                        if ($cell->getValue() != "Mostra") {
                            $test = '\'' . mysql_real_escape_string($cell->getValue()) . '\'';
                        }
                    }

                    if ($id_col == "F") {
                        if ($cell->getValue() != "Quantitat de mostra") {
                            $test_quantity = '\'' . mysql_real_escape_string($cell->getValue()) . '\'';
                        }
                    }

                    if ($id_col == "G") {
                        if ($cell->getValue() != "Tipus tub") {
                            $value = '\'' . mysql_real_escape_string($cell->getValue()) . '\'';
//                            echo strlen($test_tube_type) . "\xA";
                            if (strpos($value, 'VLOOKUP')) {
//                                print_r($cell->getOldCalculatedValue());die;
                                $test_tube_type = '\'' . mysql_real_escape_string($cell->getOldCalculatedValue()) . '\'';
                            }else{
                                $test_tube_type = '\'' . mysql_real_escape_string($cell->getValue()) . '\'';
                            }
                        }
                    }
 
                    if ($id_col == "H") {
                        if ($cell->getValue() != "T.resposta (hh:mm)") {
                            $response_time = '\'' . mysql_real_escape_string($cell->getValue()) . '\'';
                        }
                    }

                    if ($id_col == "I") {
                        if ($cell->getValue() != "Valors de referència") {
                            $reference_values = '\'' . mysql_real_escape_string($cell->getValue()) . '\'';

                        }
//                        print_r($area. ",". $code.",". $description.",".$method.",".$test.",".$test_quantity.",".$test_tube_type.",".$response_time.",".$reference_values);die();
                    }
                }
            }

            if ($area != "''" && $code != "''" && $description != "''") {
//                print_r($area. ",". $code.",". $description.",".$method.",".$test.",".$test_quantity.",".$test_tube_type.",".$response_time.",".$reference_values);die();
                $values .= "(" . $area . "," . $code . "," . $description . "," . $method . "," . $test . "," . $test_quantity . "," . $test_tube_type . "," . $response_time . "," . $reference_values . "),";
                $count += 1;
            }

            if ($count >= 300) {
                $values = rtrim($values, ',');

                $sql_insert = 'INSERT INTO cataleg (area, code, description, method, test, test_quantity, test_tube_type, response_time, reference_values)
                               VALUES ' . $values . ';';
                $m->insert_one($sql_insert);
                $count = 0;
                $values = '';
            }

        }

    }

    if ($count > 0) {
        $sql_insert = 'INSERT INTO cataleg (area, code, description, method, test, test_quantity, test_tube_type, response_time, reference_values)
                               VALUES ' . $values;
        $m->insert_one($sql_insert);
        $count = 0;
        $values = '';
    }

}


echo "Fi proces importacio", EOL;

echo '<script language="javascript">';
echo 'alert("message successfully sent")';
echo '</script>';


?>